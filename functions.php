<?php
/* 
 * Child theme functions file
 * 
 */
add_action('wp_head', 'remove_himalayas_actions');
add_action('wp_enqueue_scripts', 'bainblois_child_enqueue_styles');
add_action('himalayas_footer_copyright', 'bainblois_footer_copyright', 20);
add_action('himalayas_header_image_markup_render', 'bainblois_custom_header_markup_render');
add_filter('the_content', 'add_product_list');
add_filter('post_thumbnail_html', 'wrap_thumbnail_with_link', 10, 2);
add_filter('get_the_excerpt', 'add_email_href_to_excerpt');

/**
 * Removes actions defined by himalayas parent theme
 * 
 * @return void
 */
function remove_himalayas_actions() {
	remove_action('himalayas_footer_copyright', 'himalayas_footer_copyright');
}

/**
 * Enqueue himalayas parent theme styles
 * 
 * @return void
 */
function bainblois_child_enqueue_styles() {

	$parent_style = 'himalayas-style';

	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' ); 
	wp_enqueue_style( 'bainblois_child_style',
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version')
	);
}

/**
 * Callback to render bainblois footer
 * 
 * @return void
 */
function bainblois_footer_copyright() {
	$site_link = '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" >' . get_bloginfo( 'name', 'display' ) . '</a>';

	$default_footer_value = '<span class="copyright-text">' . sprintf( __( 'Copyright &copy; %1$s %2$s', 'himalayas' ), date( 'Y' ), $site_link ) . '</span>';

	$bainblois_footer_copyright = '<div class="copyright">'.$default_footer_value.'</div>';
	echo $bainblois_footer_copyright;
}

/**
 * Function to add unordered list of posts associated with the same category as page name 
 * 
 * @return string
 */
function add_product_list($content) {
	global $post;

	if (($post->post_type === 'page')&&(!is_front_page())) {
		$category_name = strtolower($post->post_title);
		$category_id = get_cat_ID($category_name);
		$category_posts = get_posts(
			array(
				'category'			=> $category_id,
				'posts_per_page'	=> -1,
				'orderby'			=> 'name',
				'order'				=> 'asc'
			)
		);

		if (!empty($category_posts)&&($category_id !== 0)) {
			$category_post_ul = render_category_post_list($category_posts);

			return $content . '<hr />' . $category_post_ul;;
		} else {
			return $content;
		}
	} else {
		return $content;
	}
}

function render_category_post_list($posts_array) {
	$html_ul_template = '<ul>';

	foreach ($posts_array as $post) {
		$html_ul_template .= '<li><a href="' . $post->guid . '">' . $post->post_title . '</a></li>';
	}

	$html_ul_template .= '</ul>';

	return $html_ul_template;
}

function bainblois_custom_header_markup_render() { ?>
	<div id="home" class="slider-wrapper">
		<div class="bxslider">
			<?php
			$front_page = get_page_by_title('Blois Automation');

			$himalayas_slider_description = $front_page->post_content;
			$himalayas_slider_image = get_the_post_thumbnail($front_page);

			if( !empty( $himalayas_slider_image ) ): ?>
			<div class="slides">

				<div class="parallax-overlay"> </div>

				<figure>
					<?php echo $himalayas_slider_image; ?>
				</figure>

				<div class="tg-container">

					<img width="492" height="259" class="caption-logo" src="/wp-content/themes/bainblois/img/new_logo_large.png">

					<?php if( !empty( $himalayas_slider_description ) ) { ?>
					<div class="caption-sub">
						<?php echo $himalayas_slider_description; ?>
					</div>
					<?php  } ?>
				</div>
			</div>
			<?php endif; ?>
		</div><!-- bxslider -->
	</div>
<?php }

function wrap_thumbnail_with_link($html, $post_ID) {
	$titles = [
		'Freezing',
		'Graders',
		'Other',
		'Portioning',
		'Skinning Machines',
		'Used Equipment'
	];

	$post_category = get_the_category($post_ID)[0]->name;
	$post_title = get_the_title($post_ID);

	if (($post_category === 'Blog') || (in_array($post_title, $titles))) {
		$html = '<a href="'.get_post_permalink($post_ID).'">'.$html.'</a>';
	}

	return $html;
}

function add_email_href_to_excerpt($excerpt) {
	$excerptArray = explode(' Email:', $excerpt);
	
	if ($excerptArray[1]) {
		$excerptArray[1] = str_replace("\xc2\xa0", '', $excerptArray[1]);
		$excerptArray[1] = ltrim($excerptArray[1]);
		$excerptArray[1] = '<a href=mailto:'.$excerptArray[1].'>'.$excerptArray[1].'</a>';
		return implode('<br />Email: ', $excerptArray);
	} else {
		return $excerpt;
	}
}

?>